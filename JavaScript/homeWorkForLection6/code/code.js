// Напишіть функцію isEmpty(obj), 
// яка повертає true, якщо об'єкт не має 
// властивостей, інакше false.

function isEmpty(obj) {
    for (let newObj in obj) {
      return false;
    } 
    return true;
  }