/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, 
вага, водій типу Driver, мотор типу Engine. Методи start(), 
stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", 
"Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також 
метод toString(), який виводить повну інформацію про автомобіль, 
її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що 
характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також 
характеризується граничною швидкістю.
*/

class Car {
    constructor(carbrand, 
                car_class, 
                car_weight,
                Driver, 
                Engine,
                )
                {
        this.carBrand = carbrand;
        this.carClass = car_class;
        this.carWeight = car_weight;
        this.driver = Driver;
        this.engine = Engine;
                }
        start() {
                console.log("Поїхали");
        }
        stop() {
                console.log("Зупиняємося");
        }
        turnRight() {
                console.log("Поворот праворуч");
        }
        turnLeft() {
                console.log("Поворот ліворуч");
        }
        toString() {
                console.log();
        }
}

// Клас Engine містить поля – потужність, виробник.
class Engine {
    constructor(engine_power,
                engine_manufacturer
                )
                {
        this.enginePower = engine_power;
        this.engineManufacturer = engine_manufacturer;
                }
}

// Клас Driver містить поля - ПІБ, стаж водіння.

class Driver {
    constructor(full_name,
                driving_experience
                )
                {
        this.fullName = full_name;
        this.drivingExperience = driving_experience;
                }
}