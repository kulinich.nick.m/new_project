const styles = ["Джаз", "Блюз"];

styles.push("Рок-н-ролл");
console.log(styles);

// Створюю нову змінну, надаю їй значення середини 
// масиву (масив/2 приведене у число і окрулено в меншу сторону)
const midlArray = Math.floor(styles.length / 2);
// Видаляю середній елемент і вставляю на його місце новий
styles.splice(midlArray, 1, " Класика");
// Вивожу новий масив на сторінку
document.write(styles + "<br>" + "<br>");

const newStyles = styles.shift();
console.log(newStyles);

styles.unshift("Реп", "Реггі");
console.log(styles);